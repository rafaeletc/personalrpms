%global extdir		wireless-hid@chlumskyvaclav.gmail.com
%global gschemadir	%{_datadir}/glib-2.0/schemas
 
Name:		gnome-shell-extension-wireless-hid
Version:	14
Release:	1%{?dist}
Summary:	Shows the battery of the wireless keyboards, mice, and game controllers
 
License:	MIT
URL:		https://github.com/vchlum/wireless-hid
Source0:	https://github.com/vchlum/wireless-hid/archive/refs/tags/%{version}.tar.gz#/%{name}-v%{version}.tar.gz 
 
BuildArch:	noarch
BuildRequires:	gnome-shell
BuildRequires:	gettext
BuildRequires:	%{_bindir}/glib-compile-schemas
BuildRequires:	unzip
BuildRequires:	make

Requires:	gnome-shell-extension-common

%description
wireless-hid shows the battery of the wireless keyboards, mice, and game controllers in percentage and colors. Multiple devices are supported. This extension is inspired by the Keyboard battery extension on e.g.o.
 
%prep
mkdir -p %{name}-%{version}
tar -xC %{name}-%{version} -f %{SOURCE0}
mv %{name}-%{version}/wireless-hid-%{version}/* %{name}-%{version}/
cd %{name}-%{version}
./release.sh
 
%install
mkdir -p %{buildroot}%{_datadir}/gnome-shell/extensions/%{extdir}
cd %{buildroot}%{_datadir}/gnome-shell/extensions/%{extdir}
unzip -qq %{_builddir}/%{name}-%{version}/%{extdir}.zip
mkdir -p %{buildroot}%{_datadir}/licenses/%{name}
cp %{_builddir}/%{name}-%{version}/LICENSE %{buildroot}%{_datadir}/licenses/%{name}/LICENSE

%files
%license LICENSE
%{_datadir}/gnome-shell/extensions/%{extdir}

%changelog
* Fri Aug 18 2023 Rafael de Lima Franco <rafaeletc@duck.com> - 14
- Update version
