# personalrpms

## packages

- ### [firefox-dev](https://gitlab.com/rafaeletc/personalrpms/-/tree/main/firefox-dev)    [![Copr build status](https://copr.fedorainfracloud.org/coprs/rafaeletc/personalrpms/package/firefox-dev/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/rafaeletc/personalrpms/package/firefox-dev/)

- ### [gnome-shell-extension-clipboard-history](https://gitlab.com/rafaeletc/personalrpms/-/tree/main/gnome-shell-extension-clipboard-history)    [![Copr build status](https://copr.fedorainfracloud.org/coprs/rafaeletc/personalrpms/package/gnome-shell-extension-clipboard-history/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/rafaeletc/personalrpms/package/gnome-shell-extension-clipboard-history/)

- ### [gnome-shell-extension-gamemode](https://gitlab.com/rafaeletc/personalrpms/-/tree/main/gnome-shell-extension-gamemode)    [![Copr build status](https://copr.fedorainfracloud.org/coprs/rafaeletc/personalrpms/package/gnome-shell-extension-gamemode/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/rafaeletc/personalrpms/package/gnome-shell-extension-gamemode/)

- ### [gnome-shell-extension-gesture-improvements](https://gitlab.com/rafaeletc/personalrpms/-/tree/main/gnome-shell-extension-gesture-improvements)    [![Copr build status](https://copr.fedorainfracloud.org/coprs/rafaeletc/personalrpms/package/gnome-shell-extension-gesture-improvements/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/rafaeletc/personalrpms/package/gnome-shell-extension-gesture-improvements/)

- ### [gnome-shell-extension-wireless-hid](https://gitlab.com/rafaeletc/personalrpms/-/tree/main/gnome-shell-extension-wireless-hid)    [![Copr build status](https://copr.fedorainfracloud.org/coprs/rafaeletc/personalrpms/package/gnome-shell-extension-wireless-hid/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/rafaeletc/personalrpms/package/gnome-shell-extension-wireless-hid/)

- ### [gnome-x11-gesture-daemon](https://gitlab.com/rafaeletc/personalrpms/-/tree/main/gnome-x11-gesture-daemon)    [![Copr build status](https://copr.fedorainfracloud.org/coprs/rafaeletc/personalrpms/package/gnome-x11-gesture-daemon/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/rafaeletc/personalrpms/package/gnome-x11-gesture-daemon/)

- ### [jamesdsp](https://gitlab.com/rafaeletc/personalrpms/-/tree/main/jamesdsp)    [![Copr build status](https://copr.fedorainfracloud.org/coprs/rafaeletc/personalrpms/package/jamesdsp/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/rafaeletc/personalrpms/package/jamesdsp/)

- ### [pure-data](https://gitlab.com/rafaeletc/personalrpms/-/tree/main/pure-data)    [![Copr build status](https://copr.fedorainfracloud.org/coprs/rafaeletc/personalrpms/package/pure-data/status_image/last_build.png)](https://copr.fedorainfracloud.org/coprs/rafaeletc/personalrpms/package/pure-data/)

