%global extdir      %{_datadir}/gnome-shell/extensions/clipboard-history@alexsaveau.dev
%global gschemadir  %{_datadir}/glib-2.0/schemas

%global giturl https://github.com/SUPERCILEX/gnome-clipboard-history
#%%global commit fdbd4bfb3e6bacb783304b4b36f4ad344c67a9af
#%%global commit_short %%(c=%%{commit}; echo ${c:0:7})
#%%global commit_date 20220428

Name:           gnome-shell-extension-clipboard-history
Version:        1.3.4
Release:        1%{?dist}
#Release:        5.%%{commit_date}git%%{commit_short}%%{?dist}
Summary:        Clipboard History gnome extension

License:        GPLv2+
URL:            https://github.com/SUPERCILEX/gnome-clipboard-history
%if 0%{?commit:1}
Source0:        %{giturl}/archive/%{commit}.tar.gz
%else
Source0:        %{giturl}/archive/refs/tags/%{version}.tar.gz#/gnome-clipboard-history-%{version}.tar.gz
%endif
Patch0:		enable_private_mode_default.patch

BuildArch:      noarch

BuildRequires:  gettext
BuildRequires:  make
BuildRequires:  sassc
BuildRequires:  %{_bindir}/glib-compile-schemas

Requires:       gnome-shell-extension-common
Requires:       dconf-editor
Requires:       dconf

# libdbusmenu won't be part of RHEL 9, thus disable the dependency.
%if 0%{?fedora}
Requires:       libdbusmenu-gtk3
%endif

%description
Gnome Clipboard History is a Gnome extension that saves what you've copied into an easily accessible, searchable history panel.


%prep
%if 0%{?commit:1}
%autosetup -n gnome-clipboard-history-%{commit} -p 1
%else
%autosetup -n gnome-clipboard-history-%{version} -p 1
%endif


%build
make compile-locales


%install
install -dm755 %{buildroot}%{extdir}
install -dm755 %{buildroot}%{gschemadir}
find . -regextype posix-egrep -regex ".*\.(js|json|xml|css|mo|compiled)$" -exec\
   install -Dm 644 {} %{buildroot}%{extdir}/{} \;
install -Dm 644 "schemas/org.gnome.shell.extensions.clipboard-indicator.gschema.xml" \
   "%{buildroot}%{gschemadir}/"

# Cleanup crap.
mkdir -p %{buildroot}%{gschemadir}
cp %{buildroot}%{extdir}/schemas/*gschema.xml %{buildroot}%{gschemadir}/
%{__rm} -fr %{buildroot}%{extdir}/{README*,schemas}

# Fedora handles this using triggers.
%if 0%{?rhel} && 0%{?rhel} <= 7
%postun
if [ $1 -eq 0 ] ; then
  %{_bindir}/glib-compile-schemas %{gschemadir} &> /dev/null || :
fi

%posttrans
%{_bindir}/glib-compile-schemas %{gschemadir} &> /dev/null || :
%endif

%files
%doc README.md
%{extdir}
%{gschemadir}/*gschema.xml

%changelog
* Thu May 11 2023 Rafael de Lima Franco <rafaeletc@duck.com> - 1.3.4
- Initial build
