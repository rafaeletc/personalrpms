%global giturl https://github.com/harshadgavali/gnome-x11-gesture-daemon
%global debug_package %{nil}

Name:           gnome-x11-gesture-daemon
Version:        0.2.1
Release:        2%{?dist}
Summary:        Touchpad Gestures in Gnome X11

License:        MIT
URL:            https://github.com/harshadgavali/gnome-x11-gesture-daemon
Source0:        %{giturl}/archive/refs/tags/v%{version}.tar.gz#/%{name}-v%{version}.tar.gz
Source1:	x11-gesture-daemon-user-startup
Source2:	x11-gesture-daemon-user-startup.desktop
Patch0:		0001-add-wantedby-target-to-service-unit.patch

BuildRequires:  make
BuildRequires:  rust 
BuildRequires:  libinput-devel 
BuildRequires:  cargo 
BuildRequires:  systemd-devel
BuildRequires:  wget
BuildRequires:  setconf

Requires:       libinput
Requires:       systemd
%description
A Systemd Daemon or Enabling Touchpad Gestures in Gnome X11, through libinput.


%prep
%autosetup -p1

%build
cargo fetch --locked --target "%{_arch}-unknown-linux-gnu"
setconf "gesture_improvements_gesture_daemon.service" ExecStart "/usr/bin/gesture_improvements_gesture_daemon"
export RUSTUP_TOOLCHAIN=stable
export CARGO_TARGET_DIR=target
cargo build --frozen --release --all-features

%install
mkdir -p "%{buildroot}%{_bindir}/"	
cp "target/release/gesture_improvements_gesture_daemon" "%{buildroot}%{_bindir}/gesture_improvements_gesture_daemon"
cp %{SOURCE1} "%{buildroot}%{_bindir}/x11-gesture-daemon-user-startup"

mkdir -p "%{buildroot}%{_sysconfdir}/xdg/autostart/"
cp %{SOURCE2} "%{buildroot}%{_sysconfdir}/xdg/autostart/x11-gesture-daemon-user-startup.desktop"

mkdir -p "%{buildroot}/usr/lib/systemd/user/"
cp "gesture_improvements_gesture_daemon.service" "%{buildroot}/usr/lib/systemd/user/gesture_improvements_gesture_daemon.service"

mkdir -p "%{buildroot}%{_datadir}/licenses/%{name}"
wget https://raw.githubusercontent.com/harshadgavali/gnome-x11-gesture-daemon/main/LICENSE -O %{buildroot}%{_datadir}/licenses/%{name}/LICENSE

%post
touch /etc/adduser.conf  || echo "/etc/adduser.conf already exists !"
echo 'ADD_EXTRA_GROUPS=1' > /etc/adduser.conf
echo 'EXTRA_GROUPS=input' >> /etc/adduser.conf
usermod -a -G input $LOGNAME
%files
%attr(0755, root, root) %{_bindir}/gesture_improvements_gesture_daemon
%attr(0755, root, root) %{_bindir}/x11-gesture-daemon-user-startup
%attr(0644, root, root) %{_datadir}/licenses/%{name}/LICENSE
%attr(0755, root, root) %{_sysconfdir}/xdg/autostart/x11-gesture-daemon-user-startup.desktop
%attr(0644, root, root) /usr/lib/systemd/user/gesture_improvements_gesture_daemon.service
