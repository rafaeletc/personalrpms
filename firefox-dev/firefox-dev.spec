%global             source_name firefox
%global             application_name firefox-dev

Name:               firefox-dev
Version:            121.0b9
Release:            1%{?dist}
Summary:            Firefox Developer Edition (formerly "Aurora") pre-beta Web browser

License:            MPLv1.1 or GPLv2+ or LGPLv2+
URL:                https://www.mozilla.org/pt-BR/firefox/developer/
Source0:            https://download-installer.cdn.mozilla.net/pub/devedition/releases/%{version}/linux-x86_64/pt-BR/firefox-%{version}.tar.bz2
Source1:            firefox-developer-edition.desktop
Source2:            policies.json

ExclusiveArch:      x86_64

Requires(post):     xdg-utils
Requires(post):     gtk-update-icon-cache

%description
This is a pre-beta release of Mozilla Firefox intended for Web developers and
enthusiasts who want early access to new features. It receives new updates
(almost) daily, adding and refining support for the very latest Web standards
that won't make it into the stable release of Firefox for some months. It also
comes with some addons for Web development enabled by default.

You may actually find that Developer Edition works just fine for normal everyday
use: Some users set it as their default Web browser. You can sign in to your
normal Firefox account and sync your preferences, extensions, and bookmarks,
etc. Or you can keep the Firefox versions separate, and use different profiles,
even different browser UI themes. Firefox Developer Edition can install
alongside the stable release of Firefox, making it easy to switch back and forth
between the two versions.

That being said, a lot of the technology here is still experimental, and there
will very likely be some bugs, so just remember that by using Developer Edition,
you're helping Mozilla make Firefox the best Web browser they can. And have fun!

Bugs related to Firefox Developer Edition should be reported directly to Mozilla:
<https://bugzilla.mozilla.org/>

Bugs related to this package should be reported at this GitHub project:
<https://github.com/the4runner/firefox-dev/issues/>

%prep
%setup -q -n %{source_name}

%install
%__rm -rf %{buildroot}

%__install -d %{buildroot}{/opt/%{application_name},%{_bindir},%{_datadir}/applications}

%__cp -r * %{buildroot}/opt/%{application_name}

%__ln_s /opt/%{application_name}/firefox %{buildroot}%{_bindir}/%{application_name}

%__install -D -m 0644 %{SOURCE1} -t %{buildroot}%{_datadir}/applications

%__install -D -m 0444 %{SOURCE2} -t %{buildroot}/opt/%{application_name}/distribution


%post
xdg-icon-resource install --novendor --size 128 /opt/firefox-dev/browser/chrome/icons/default/default128.png firefox-developer-edition
gtk-update-icon-cache -f -t /usr/share/icons/hicolor

%files
%{_datadir}/applications/firefox-developer-edition.desktop
%{_bindir}/%{application_name}
/opt/%{application_name}

%changelog
* Fri Dec 08 2023 Rafael de Lima Franco <rafaeletc@duck.com> - 121.0b9
- Update version

* Tue Nov 28 2023 Rafael de Lima Franco <rafaeletc@duck.com> - 121.0b4
- Update version

* Tue Nov 21 2023 Rafael de Lima Franco <rafaeletc@duck.com> - 121.0b1
- Update version

* Mon Nov 20 2023 Rafael de Lima Franco <rafaeletc@duck.com> - 120.0b9
- Update version

* Wed Nov 09 2023 Rafael de Lima Franco <rafaeletc@duck.com> - 120.0b8
- Update version

* Wed Nov 08 2023 Rafael de Lima Franco <rafaeletc@duck.com> - 120.0b7
- Update version

* Thu Nov 02 2023 Rafael de Lima Franco <rafaeletc@duck.com> - 120.0b5
- Update version

* Thu Oct 26 2023 Rafael de Lima Franco <rafaeletc@duck.com> - 120.0b2
- Update version

* Fri Oct 13 2023 Rafael de Lima Franco <rafaeletc@duck.com> - 119.0b8 
- Update version

* Sat Sep 02 2023 Rafael de Lima Franco <rafaeletc@duck.com> - 118.0b3
- Update version

* Fri Aug 18 2023 Rafael de Lima Franco <rafaeletc@duck.com> - 117.0b9
- Update version

* Tue Aug 15 2023 Rafael de Lima Franco <rafaeletc@duck.com> - 117.0b7
- Update version

* Sat Jul 29 2023 Rafael de Lima Franco <rafaeletc@duck.com> - 116.0b8
- Update version

* Sat Jul 15 2023 Rafael de Lima Franco <rafaeletc@duck.com> - 116.0b5
- Update version

* Thu May 11 2023 Rafael de Lima Franco <rafaeletc@duck.com> - 114.0b2
- Update version

* Sat May 06 2023 Rafael de Lima Franco <rafaeletc@duck.com> - 113.0b9
- Initial build
