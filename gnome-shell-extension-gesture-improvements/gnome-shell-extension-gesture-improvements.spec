%global extdir		gestureImprovements@gestures
%global gschemadir	%{_datadir}/glib-2.0/schemas
 
Name:		gnome-shell-extension-gesture-improvements
Version:	0.2.1
Release:	3%{?dist}
Summary:	This extension modifies and extends existing touchpad gestures on GNOME.
 
License:	GPLv3
URL:		https://github.com/harshadgavali/gnome-gesture-improvements
 
BuildArch:	noarch
 
BuildRequires:	gettext
BuildRequires:	nodejs-npm
BuildRequires:	make
BuildRequires:	git
BuildRequires:	%{_bindir}/glib-compile-schemas	
 
Requires:	gnome-shell-extension-common
Requires:	gnome-x11-gesture-daemon

%description
Improve touchpad gestures for Wayland/X11
This extension adds following features:
• Switch windows from current workspace using 3-finger horizontal swipe
• Cyclic gestures between Desktop/Overview/AppGrid using 4 vertical swipe
• Switch app-pages using 3-finger swipe gesture on AppGrid
• Unmaximize/maximize/fullscreen/half-tiling using 3-finger vertical & horizontal gesture
• Optional minimize a window gesture
• Override 3-finger gesture with 4-finger for switching workspace
• Pinch to show desktop
• Application specific keyboard shortcut based hold-swipe gestures (e.g., navigating browser tabs)
• Configure speed of gestures
• Support for X11
On X11, you need to install https://github.com/harshadgavali/gnome-x11-gesture-daemon
Report any bugs/requests on GitHub
 
%prep
git clone https://github.com/harshadgavali/gnome-gesture-improvements || echo
cd gnome-gesture-improvements

%build
cd %{_builddir}/gnome-gesture-improvements
export npm_config_cache="./npm_cache"
npm install
npm run pack

%install
mkdir -p %{buildroot}%{_datadir}/gnome-shell/extensions/%{extdir}
cd %{buildroot}%{_datadir}/gnome-shell/extensions/%{extdir}
unzip -qq %{_builddir}/gnome-gesture-improvements/build/%{extdir}.shell-extension.zip
mkdir -p %{buildroot}%{_datadir}/licenses/%{name}
cp %{_builddir}/gnome-gesture-improvements/LICENSE %{buildroot}%{_datadir}/licenses/%{name}/LICENSE
mkdir -p %{buildroot}/%{gschemadir}
cp %{buildroot}%{_datadir}/gnome-shell/extensions/%{extdir}/schemas/*.xml %{buildroot}/%{gschemadir}/
# Fedora handles this using triggers.
%if 0%{?rhel} && 0%{?rhel} <= 7
%postun
if [ $1 -eq 0 ] ; then
  %{_bindir}/glib-compile-schemas %{gschemadir} &> /dev/null || :
fi


%posttrans
%{_bindir}/glib-compile-schemas %{gschemadir} &> /dev/null || :
%endif

%files
%license LICENSE
%{_datadir}/gnome-shell/extensions/%{extdir}
%{gschemadir}/*.xml
